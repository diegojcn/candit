package br.com.estoque.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Before;

/**
 * 
 * @author d.neves
 *
 */
public abstract class TestEstoqueUtil {

	private WebTarget target;

	public static final String PATH_SISTEMA = "http://localhost:8080/estoque-ws/app";

	@Before
	public void preparar() {
		Client client = ClientBuilder.newClient();
		target = client.target(PATH_SISTEMA);

		setUpDoTeste();
	}

	public void finalizar() {
		finalize();
	}

	public abstract void finalize();

	public abstract void setUpDoTeste();

	/**
	 * @return the target
	 */
	public WebTarget getTarget() {
		return target;
	}

	/**
	 * @param target
	 *            the target to set
	 */
	public void setTarget(WebTarget target) {
		this.target = target;
	}

}
