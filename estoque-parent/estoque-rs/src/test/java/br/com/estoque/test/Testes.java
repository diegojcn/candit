package br.com.estoque.test;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.Test;

import br.com.estoque.domain.TipoBebidaEnum;
import br.com.estoque.ws.vo.BebidaVo;
import br.com.estoque.ws.vo.EstoqueVo;
import br.com.estoque.ws.vo.HistoricoVo;

/**
 * 
 * @author d.neves
 *
 */
public class Testes extends TestEstoqueUtil {

	public static final Logger LOG = Logger.getLogger(Testes.class.getName());

	@Override
	public void setUpDoTeste() {

	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub

	}

	@Test
	public void incluirEstoque() {

		EstoqueVo vo = new EstoqueVo();
		vo.setNome("Estoque " + RandomUtils.nextLong());
		vo.setTotalDeSecoes(5);

		Response post = getTarget().path("/estoque/salvarEstoque").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(vo, MediaType.APPLICATION_JSON));

		Assert.assertEquals(Status.fromStatusCode(post.getStatus()), Status.OK);

	}

	@Test
	public void incluirBebida() {

		BebidaVo vo = new BebidaVo();
		vo.setNome("Faxe 10%");
		vo.setQuantidade(100);
		vo.setTipoBebida(TipoBebidaEnum.ALCOLICA.getCodigo());

		Response post = getTarget().path("/estoque/incluirBebida").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(vo, MediaType.APPLICATION_JSON));

		Assert.assertEquals(Status.fromStatusCode(post.getStatus()), Status.OK);

	}

	@Test
	public void incluirBebidaDemais() {

		BebidaVo vo = new BebidaVo();
		vo.setNome("Faxe 10%");
		vo.setQuantidade(2000000);
		vo.setTipoBebida(TipoBebidaEnum.ALCOLICA.getCodigo());

		Response post = getTarget().path("/estoque/incluirBebida").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(vo, MediaType.APPLICATION_JSON));

		Assert.assertEquals(Status.fromStatusCode(post.getStatus()), Status.BAD_REQUEST);

	}

	@Test
	public void pesquisarHistorico() {

		Response post = getTarget().path("/historico/obterSecaoHistorico/1").request()
				.accept(MediaType.APPLICATION_JSON).get();

		List<HistoricoVo> readEntity = post.readEntity(List.class);
		Assert.assertEquals(Status.fromStatusCode(post.getStatus()), Status.OK);

	}

}
