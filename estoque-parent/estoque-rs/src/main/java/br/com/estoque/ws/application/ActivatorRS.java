package br.com.estoque.ws.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author d.neves
 *
 */
@ApplicationPath("/app")
public class ActivatorRS extends Application {

}
