package br.com.estoque.ws.vo;

import java.io.Serializable;

import br.com.estoque.domain.generic.EntidadeEstoque;
import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * @author d.neves
 *
 */
public class ExceptionVo extends GenericVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4865186461488179100L;

	public final String message;

	public ExceptionVo(Exception e) {

		this.message = e.getMessage();

	}

	@Override
	public <T extends EntidadeEstoque> T obterObjeto() {
		// TODO Auto-generated method stub
		return null;
	}

}
