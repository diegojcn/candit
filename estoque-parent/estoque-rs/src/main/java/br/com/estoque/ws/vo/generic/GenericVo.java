package br.com.estoque.ws.vo.generic;

import br.com.estoque.domain.generic.EntidadeEstoque;

public abstract class GenericVo {

	public abstract <T extends EntidadeEstoque> T obterObjeto();

}
