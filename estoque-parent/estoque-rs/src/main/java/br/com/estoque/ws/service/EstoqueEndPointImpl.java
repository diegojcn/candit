package br.com.estoque.ws.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.core.Response;

import br.com.estoque.dao.IBebidaDAOLocal;
import br.com.estoque.dao.IEstoqueDAOLocal;
import br.com.estoque.dao.IHistorioDAOLocal;
import br.com.estoque.dao.ISecaoDAOLocal;
import br.com.estoque.domain.Bebida;
import br.com.estoque.domain.Estoque;
import br.com.estoque.domain.Historico;
import br.com.estoque.domain.Secao;
import br.com.estoque.domain.TipoBebidaEnum;
import br.com.estoque.domain.TipoMovimentacaoEnum;
import br.com.estoque.exception.EstoqueException;
import br.com.estoque.ws.IEstoqueEndPoint;
import br.com.estoque.ws.service.util.GenericServiceEuviiImpl;
import br.com.estoque.ws.vo.BebidaVo;
import br.com.estoque.ws.vo.EstoqueVo;

/**
 * 
 * @author d.neves
 *
 */

@Singleton
public class EstoqueEndPointImpl extends GenericServiceEuviiImpl implements IEstoqueEndPoint {

	@EJB
	private IEstoqueDAOLocal estoqueDAO;

	@EJB
	private ISecaoDAOLocal secaoDAO;

	@EJB
	private IBebidaDAOLocal bebidaDAO;

	@EJB
	private IHistorioDAOLocal historicoDAO;

	@Override
	public Response incluirBebida(BebidaVo bebidaVo) throws EstoqueException {

		TipoBebidaEnum tipoBebidaEnum = TipoBebidaEnum.getTipoPorCodigo(bebidaVo.getTipoBebida());
		Integer contarCapacidadePorBebida = secaoDAO.contarCapacidadePorBebida(tipoBebidaEnum);

		if (contarCapacidadePorBebida < bebidaVo.getQuantidade()) {
			throw new EstoqueException(
					"Não há capacidade na seção para cadastrar esta quantidade de bebida, cadastre outro estoque.");
		}

		List<Secao> lista = secaoDAO.pesquisarEstoqueComSecaoDisponivel(bebidaVo.getQuantidade(), tipoBebidaEnum);

		List<Historico> listaParaSalvarHist = new ArrayList<Historico>();

		for (Secao item : lista) {

			int capacidadeIndex = item.getCapacidadeDisponivel() - bebidaVo.getQuantidade();
			Bebida bebida = bebidaVo.obterObjeto();
			Integer quantidade = bebidaVo.getQuantidade();
			if (capacidadeIndex >= 0) {

				item.setCapacidadeDisponivel(capacidadeIndex);
				bebidaVo.setQuantidade(0);

			} else {
				quantidade = bebidaVo.getQuantidade() - item.getCapacidadeDisponivel();
				bebidaVo.setQuantidade(quantidade);
				item.setCapacidadeDisponivel(0);

			}

			bebida.setSecao(item);
			Historico h = new Historico(TipoMovimentacaoEnum.ENTRADA, bebida, "teste", quantidade);
			secaoDAO.salvar(item);
			listaParaSalvarHist.add(h);
			if (bebidaVo.getQuantidade() == 0) {
				break;
			}

		}

		for (Historico item : listaParaSalvarHist) {

			Historico salvo = historicoDAO.salvar(item);
		}
		return responseOk(bebidaVo, JSON);

	}

	@Override
	public Response salvarEstoque(EstoqueVo estoqueVo) throws EstoqueException {

		Estoque estoque = estoqueVo.obterObjeto();

		estoque = estoqueDAO.salvar(estoque);

		for (int i = 0; i < estoqueVo.getTotalDeSecoes(); i++) {

			TipoBebidaEnum tipo = i % 2 == 0 ? TipoBebidaEnum.NAOALCOLICA : TipoBebidaEnum.ALCOLICA;

			Secao secao = new Secao(tipo, estoque);
			secaoDAO.salvar(secao);
		}

		return responseOk(estoqueVo, JSON);
	}

}
