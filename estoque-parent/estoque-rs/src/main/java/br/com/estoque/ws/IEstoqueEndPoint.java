package br.com.estoque.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.estoque.exception.EstoqueException;
import br.com.estoque.ws.vo.BebidaVo;
import br.com.estoque.ws.vo.EstoqueVo;

/**
 * 
 * @author d.neves
 *
 */

@Path("/estoque")
public interface IEstoqueEndPoint {

	/**
	 * http://localhost:8080/estoque-ws/app/estoque/incluirBebida/
	 * 
	 * @param bebidaVo
	 * @return
	 */
	@POST
	@Path("/incluirBebida")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	Response incluirBebida(BebidaVo bebidaVo) throws EstoqueException;

	/**
	 * http://localhost:8080/estoque-ws/app/estoque/salvarEstoque/
	 * 
	 * @param estoqueVo
	 * @return
	 */

	@POST
	@Path("/salvarEstoque")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	Response salvarEstoque(EstoqueVo estoqueVo) throws EstoqueException;

}