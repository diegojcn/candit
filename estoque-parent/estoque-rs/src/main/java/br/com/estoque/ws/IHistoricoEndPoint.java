package br.com.estoque.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.estoque.exception.EstoqueException;
import br.com.estoque.ws.vo.HistoricoVo;

/**
 * 
 * @author d.neves
 *
 */

@Path("/historico")
public interface IHistoricoEndPoint {

	@GET
	@Path("/obterSecaoHistorico/{tipoBebida}")
	@Produces("application/json")
	List<HistoricoVo> obterSecaoHistorico(@PathParam("tipoBebida") int tipoBebida) throws EstoqueException;

}