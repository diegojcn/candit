package br.com.estoque.ws.service.util;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * 
 * @author d.neves
 *
 */
public abstract class GenericServiceEuviiImpl {

	public static final String JSON = "application/json";

	public static final String XML = "text/xml";

	/**
	 * Response de objeto.
	 * 
	 * @param objClient
	 * @param status
	 * @param type
	 * @return
	 */
	public <O extends GenericVo> Response response(O objClient, Status status, String type) {

		Response build = Response.status(status.getStatusCode()).entity(objClient).type(type).build();

		return build;
	}

	/**
	 * Response de lista de objetos.
	 * 
	 * @param objClient
	 * @param status
	 * @param type
	 * @return
	 */
	public <O extends GenericVo> Response response(List<O> objClient, Status status, String type) {

		Response build = Response.status(status.getStatusCode()).entity(objClient).type(type).build();

		return build;
	}

	/**
	 * Response.
	 * 
	 * @param objetct
	 * @return
	 */
	public <T extends GenericVo> Response responseOk(T objetct, String tipo) {
		Response reseponse = buildResponOk(objetct, tipo);
		return reseponse;
	}

	/**
	 * Metodo para criar o response OK
	 * 
	 * @param object
	 * @param tipo
	 * @return
	 */
	private Response buildResponOk(Object object, String tipo) {
		ResponseBuilder ok = Response.ok();
		ResponseBuilder entity = ok.entity(object);
		if (tipo != null) {
			entity = entity.type(tipo);
		}
		Response reseponse = entity.build();
		return reseponse;
	}

	/**
	 * Response.
	 * 
	 * @param list
	 * @return
	 */
	public <T extends GenericVo> Response responseListOk(List<T> list, String tipo) {
		Response reseponse = buildResponOk(list, tipo);
		return reseponse;
	}

}
