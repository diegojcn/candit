package br.com.estoque.ws.vo;

import br.com.estoque.domain.Historico;
import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * @author Diego Neves
 *
 */
public class HistoricoVo extends GenericVo {

	private Long id;

	private BebidaVo bebidaVo;

	private SecaoVo secaoVo;

	private Integer quantidade;

	private String responsavel;

	private String tipoMovimentacao;

	public HistoricoVo(Historico item) {
		this.secaoVo = new SecaoVo(item.getSecao());
		this.bebidaVo = new BebidaVo(item.getBebida());
		this.quantidade = item.getQuantidade();
		this.responsavel = item.getResponsavel();
		this.tipoMovimentacao = item.getTipoMovimentacaoEnum().getDescricao();
	}

	@Override
	public Historico obterObjeto() {

		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BebidaVo getBebidaVo() {
		return bebidaVo;
	}

	public void setBebidaVo(BebidaVo bebidaVo) {
		this.bebidaVo = bebidaVo;
	}

	public SecaoVo getSecaoVo() {
		return secaoVo;
	}

	public void setSecaoVo(SecaoVo secaoVo) {
		this.secaoVo = secaoVo;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getTipoMovimentacao() {
		return tipoMovimentacao;
	}

	public void setTipoMovimentacao(String tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}

}
