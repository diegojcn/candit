package br.com.estoque.ws.vo;

import java.util.ArrayList;

import br.com.estoque.domain.Estoque;
import br.com.estoque.domain.Secao;
import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * @author Diego Neves
 *
 */
public class EstoqueVo extends GenericVo {

	private Long id;

	private String nome;
	
	private Integer totalDeSecoes;

	@Override
	public Estoque obterObjeto() {

		Estoque e = new Estoque();
		e.setNome(this.nome);
		e.setSecoes(new ArrayList<Secao>());

		return e;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTotalDeSecoes() {
		return totalDeSecoes;
	}

	public void setTotalDeSecoes(Integer totalDeSecoes) {
		this.totalDeSecoes = totalDeSecoes;
	}

}
