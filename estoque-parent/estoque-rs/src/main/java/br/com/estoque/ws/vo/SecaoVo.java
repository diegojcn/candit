package br.com.estoque.ws.vo;

import br.com.estoque.domain.Secao;
import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * @author Diego Neves
 *
 */
public class SecaoVo extends GenericVo {

	private Long id;

	private Integer capacidadeDisponivel;

	private Integer tipoBebida;
	
	public SecaoVo(Secao secao) {
		this.id = secao.getCodigo();
		this.capacidadeDisponivel = secao.getCapacidadeDisponivel();
		this.tipoBebida = secao.getTipoBebida();
	}

	@Override
	public Secao obterObjeto() {

		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCapacidadeDisponivel() {
		return capacidadeDisponivel;
	}

	public void setCapacidadeDisponivel(Integer capacidadeDisponivel) {
		this.capacidadeDisponivel = capacidadeDisponivel;
	}

	public Integer getTipoBebida() {
		return tipoBebida;
	}

	public void setTipoBebida(Integer tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

}
