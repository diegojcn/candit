package br.com.estoque.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * 
 * @author d.neves
 *
 */
@Path("/init")
public interface IIinicializadorEndPoint {
	/**
	 * Inicializador de banco de dados.
	 * 
	 * @return
	 */
	@GET
	@Path("/banco")
	String inicializarBanco();

}