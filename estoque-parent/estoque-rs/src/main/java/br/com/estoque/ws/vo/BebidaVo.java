package br.com.estoque.ws.vo;

import br.com.estoque.domain.Bebida;
import br.com.estoque.ws.vo.generic.GenericVo;

/**
 * @author Diego Neves
 *
 */
public class BebidaVo extends GenericVo {

	private Long id;

	private Integer tipoBebida;

	private String nome;

	private Integer quantidade;

	public BebidaVo() {
	}

	public BebidaVo(Bebida bebida) {
		this.tipoBebida = bebida.getTipoBebida();
		this.nome = bebida.getNome();
		this.id = bebida.getCodigo();
	}

	@Override
	public Bebida obterObjeto() {

		Bebida b = new Bebida();
		b.setId(this.id);
		b.setNome(this.nome);
		b.setTipoBebida(tipoBebida);

		return b;
	}

	public Integer getTipoBebida() {
		return tipoBebida;
	}

	public void setTipoBebida(Integer tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
