package br.com.estoque.ws.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import br.com.estoque.dao.IBebidaDAOLocal;
import br.com.estoque.dao.IEstoqueDAOLocal;
import br.com.estoque.dao.IHistorioDAOLocal;
import br.com.estoque.dao.ISecaoDAOLocal;
import br.com.estoque.domain.Historico;
import br.com.estoque.domain.TipoBebidaEnum;
import br.com.estoque.exception.EstoqueException;
import br.com.estoque.ws.IHistoricoEndPoint;
import br.com.estoque.ws.service.util.GenericServiceEuviiImpl;
import br.com.estoque.ws.vo.HistoricoVo;

/**
 * 
 * @author d.neves
 *
 */

@Singleton
public class HistoricoEstoqueEndPointImpl extends GenericServiceEuviiImpl implements IHistoricoEndPoint {

	@EJB
	private IHistorioDAOLocal historicoDAO;

	@Override
	public List<HistoricoVo> obterSecaoHistorico(int tipoBebida) throws EstoqueException {

		List<Historico> lista = historicoDAO
				.pesquisarHistoricoPorTipoBebida(TipoBebidaEnum.getTipoPorCodigo(tipoBebida));

		List<HistoricoVo> h = new ArrayList<HistoricoVo>();

		for (Historico item : lista) {
			h.add(new HistoricoVo(item));
		}

		return h;
	}

}
