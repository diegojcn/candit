/**
 * 
 */
package br.com.estoque.ws.application;

import javax.ejb.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.estoque.exception.EstoqueException;
import br.com.estoque.exception.ValidationException;
import br.com.estoque.ws.vo.ExceptionVo;

/**
 * @author d.neves
 *
 */
@Provider
@Singleton
public class EstoqueExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {

		ExceptionVo entity = null;

		if (exception instanceof EstoqueException) {

			entity = new ExceptionVo((EstoqueException) exception);
			return Response.status(400).entity(entity).build();
		}

		if (exception instanceof ValidationException) {

			entity = new ExceptionVo((ValidationException) exception);
			return Response.status(400).entity(entity).build();
		}

		entity = new ExceptionVo(exception);
		return Response.status(400).entity(entity).build();

	}

}
