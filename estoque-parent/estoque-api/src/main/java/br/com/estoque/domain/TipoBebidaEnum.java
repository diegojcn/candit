package br.com.estoque.domain;

/**
 * 
 * @author Diego Neves
 *
 */
public enum TipoBebidaEnum {

	ALCOLICA(1, "Alcólica", 500),

	NAOALCOLICA(2, "Não Alcólica", 400);

	private Integer codigo;

	private String descricao;

	private Integer capacidade;

	private TipoBebidaEnum(Integer codigo, String descricao, Integer capacidade) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.capacidade = capacidade;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}

	public static TipoBebidaEnum getTipoPorCodigo(Integer tipoBebida) {

		for (TipoBebidaEnum item : TipoBebidaEnum.values()) {
			if (item.getCodigo().equals(tipoBebida)) {
				return item;
			}
		}
		return null;
	}

}
