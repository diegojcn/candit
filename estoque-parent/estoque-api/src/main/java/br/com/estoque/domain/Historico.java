package br.com.estoque.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.estoque.domain.generic.EntidadeEstoque;

/**
 * 
 * @author Diego Neves
 *
 */
@Entity
@SequenceGenerator(name = "Historico_seq", sequenceName = "Historico_seq", allocationSize = 1)
public class Historico extends EntidadeEstoque {

	private static final long serialVersionUID = 8109167073080303870L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Historico_seq")
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	private Secao secao;

	@ManyToOne(cascade = CascadeType.ALL)
	private Bebida bebida;

	private Integer quantidade;

	private Date dataHora;

	private String responsavel;

	private Integer tipoMoviemntacao;

	public Historico() {
		// TODO Auto-generated constructor stub
	}

	public Historico(TipoMovimentacaoEnum movimentacao, Bebida bebida, String responsavel, Integer quantidade) {

		this.dataHora = new Date();
		this.secao = bebida.getSecao();
		this.quantidade = quantidade;
		this.responsavel = responsavel;
		this.bebida = bebida;
		this.tipoMoviemntacao = movimentacao.getCodigo();
	}

	@Override
	public Serializable getId() {
		return id;
	}

	public Secao getSecao() {
		return secao;
	}

	public void setSecao(Secao secao) {
		this.secao = secao;
	}

	public Bebida getBebida() {
		return bebida;
	}

	public void setBebida(Bebida bebida) {
		this.bebida = bebida;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Integer getTipoMoviemntacao() {
		return tipoMoviemntacao;
	}
	
	public TipoMovimentacaoEnum getTipoMovimentacaoEnum() {
		return TipoMovimentacaoEnum.getTipoPorCodigo(tipoMoviemntacao);
	}

	public void setTipoMoviemntacao(Integer tipoMoviemntacao) {
		this.tipoMoviemntacao = tipoMoviemntacao;
	}

	public void setTipoMoviemntacao(TipoMovimentacaoEnum tipoMoviemntacao) {
		this.tipoMoviemntacao = tipoMoviemntacao.getCodigo();
	}

}
