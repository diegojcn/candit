package br.com.estoque.dao;

import javax.ejb.Local;

import br.com.estoque.dao.generic.IEstoqueGenericDAOLocal;
import br.com.estoque.domain.Estoque;

/**
 * 
 * @author Diego Neves
 *
 */
@Local
public interface IEstoqueDAOLocal extends IEstoqueGenericDAOLocal<Estoque, Long> {

}
