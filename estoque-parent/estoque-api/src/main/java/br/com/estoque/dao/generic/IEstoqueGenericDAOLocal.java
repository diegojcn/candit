package br.com.estoque.dao.generic;

import java.io.Serializable;

import javax.ejb.Local;

import br.com.dnframework.api.dao.GenericDAOLocal;
import br.com.estoque.domain.generic.EntidadeEstoque;

/**
 * @author Diego Neves
 *
 */
@Local
public interface IEstoqueGenericDAOLocal<T extends EntidadeEstoque, ID extends Serializable>
		extends GenericDAOLocal<T, ID> {

}
