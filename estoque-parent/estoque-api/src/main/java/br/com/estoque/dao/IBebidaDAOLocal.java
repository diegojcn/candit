package br.com.estoque.dao;

import javax.ejb.Local;

import br.com.estoque.dao.generic.IEstoqueGenericDAOLocal;
import br.com.estoque.domain.Bebida;

/**
 * 
 * @author Diego Neves
 *
 */
@Local
public interface IBebidaDAOLocal extends IEstoqueGenericDAOLocal<Bebida, Long> {

}
