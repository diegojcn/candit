package br.com.estoque.domain;

/**
 * 
 * @author Diego Neves
 *
 */
public enum TipoMovimentacaoEnum {

	ENTRADA(1, "Alcólica"),

	SAIDA(2, "Não Alcólica");

	private Integer codigo;

	private String descricao;

	private TipoMovimentacaoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public static TipoMovimentacaoEnum getTipoPorCodigo(Integer tipoBebida) {

		for (TipoMovimentacaoEnum item : TipoMovimentacaoEnum.values()) {
			if (item.getCodigo().equals(tipoBebida)) {
				return item;
			}
		}
		return null;
	}

}
