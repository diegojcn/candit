/**
 * 
 */
package br.com.estoque.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.estoque.domain.generic.EntidadeEstoque;

/**
 * @author Diego Neves
 *
 */
@Entity
@SequenceGenerator(name = "Secao_seq", sequenceName = "Estoque_seq", allocationSize = 1)
public class Secao extends EntidadeEstoque {

	private static final long serialVersionUID = -1638706247117675544L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Secao_seq")
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	private Estoque estoque;

	private Integer capacidadeDisponivel;

	private Integer tipoBebida;

	public Secao() {
	}

	public Secao(TipoBebidaEnum tipoBebida, Estoque estoque) {
		this.capacidadeDisponivel = tipoBebida.getCapacidade();
		this.tipoBebida = tipoBebida.getCodigo();
		this.estoque = estoque;
	}

	@Override
	public Serializable getId() {
		return id;
	}

	public Long getCodigo() {
		return id;
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTipoBebida() {
		return tipoBebida;
	}

	public void setTipoBebida(Integer tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

	public void setTipoBebida(TipoBebidaEnum tipoBebida) {
		this.tipoBebida = tipoBebida.getCodigo();
	}

	public Integer getCapacidadeDisponivel() {
		return capacidadeDisponivel;
	}

	public void setCapacidadeDisponivel(Integer capacidadeDisponivel) {
		this.capacidadeDisponivel = capacidadeDisponivel;
	}

}
