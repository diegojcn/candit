package br.com.estoque.dao;

import java.util.List;

import javax.ejb.Local;

import br.com.estoque.dao.generic.IEstoqueGenericDAOLocal;
import br.com.estoque.domain.Secao;
import br.com.estoque.domain.TipoBebidaEnum;

/**
 * 
 * @author Diego Neves
 *
 */
@Local
public interface ISecaoDAOLocal extends IEstoqueGenericDAOLocal<Secao, Long> {

	public List<Secao> pesquisarEstoqueComSecaoDisponivel(Integer capaciadadeDisponvivel,
			TipoBebidaEnum tipoBebidaEnum);

	public Integer contarCapacidadePorBebida(TipoBebidaEnum tipoBebidaEnum);

}
