package br.com.estoque.dao;

import java.util.List;

import javax.ejb.Local;

import br.com.estoque.dao.generic.IEstoqueGenericDAOLocal;
import br.com.estoque.domain.Historico;
import br.com.estoque.domain.TipoBebidaEnum;

/**
 * 
 * @author Diego Neves
 *
 */
@Local
public interface IHistorioDAOLocal extends IEstoqueGenericDAOLocal<Historico, Long> {

	List<Historico> pesquisarHistoricoPorTipoBebida(TipoBebidaEnum tipoBebida);

}
