package br.com.estoque.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.estoque.domain.generic.EntidadeEstoque;

/**
 * @author Diego Neves
 *
 */
@Entity
@SequenceGenerator(name = "Estoque_seq", sequenceName = "Estoque_seq", allocationSize = 1)
public class Estoque extends EntidadeEstoque {

	private static final long serialVersionUID = -5748395835454961601L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Estoque_seq")
	private Long id;

	private String nome;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Secao> secoes;

	public Estoque() {
	}

	@Override
	public Serializable getId() {
		return id;
	}

	public List<Secao> getSecoes() {
		return secoes;
	}

	public void setSecoes(List<Secao> secoes) {
		this.secoes = secoes;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigoEstoque() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
