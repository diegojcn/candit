package br.com.estoque.domain.generic;

import javax.persistence.MappedSuperclass;

import br.com.dnframework.api.dominio.Entidade;

/**
 * @author Diego Neves
 *
 */
@MappedSuperclass
public abstract class EntidadeEstoque extends Entidade {

	private static final long serialVersionUID = 8900980960741105600L;

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof Entidade) {
			Entidade e = (Entidade) obj;

			if (e.getId() == null || this.getId() == null) {
				return false;
			}

			if (e.getId().equals(this.getId())) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
