/**
 * 
 */
package br.com.estoque.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.estoque.domain.generic.EntidadeEstoque;

/**
 * @author Diego Neves
 *
 */
@Entity
@SequenceGenerator(name = "Bebida_seq", sequenceName = "Bebida_seq", allocationSize = 1)
public class Bebida extends EntidadeEstoque {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1591821972968311849L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Bebida_seq")
	private Long id;

	private Integer tipoBebida;

	private String nome;

	@ManyToOne(cascade = CascadeType.ALL)
	private Secao secao;

	public Bebida() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.dnframework.api.dominio.Entidade#getId()
	 */
	@Override
	public Serializable getId() {
		return id;
	}

	public Long getCodigo() {
		return id;
	}

	public Integer getTipoBebida() {
		return tipoBebida;
	}

	public void setTipoBebida(Integer tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

	public void setTipoBebida(TipoBebidaEnum tipoBebida) {
		this.tipoBebida = tipoBebida.getCodigo();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Secao getSecao() {
		return secao;
	}

	public void setSecao(Secao secao) {
		this.secao = secao;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
