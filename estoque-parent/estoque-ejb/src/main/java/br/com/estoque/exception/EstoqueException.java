package br.com.estoque.exception;

/**
 * @author d.neves
 *
 */
public class EstoqueException extends Exception {

	private static final long serialVersionUID = -796842429032788311L;


	public EstoqueException(String message) {

		super(message);
	}

	public EstoqueException(String message, Throwable cause) {

		super(message, cause);
	}

	public EstoqueException(Throwable cause) {

		super(cause);
	}


}
