package br.com.estoque.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.estoque.dao.generic.EstoqueGenericDAOImpl;
import br.com.estoque.domain.Historico;
import br.com.estoque.domain.TipoBebidaEnum;

/**
 * 
 * @author Diego Neves
 *
 */
@Stateless
public class HistoricoDAOImpl extends EstoqueGenericDAOImpl<Historico, Long> implements IHistorioDAOLocal {

	@Override
	public List<Historico> pesquisarHistoricoPorTipoBebida(TipoBebidaEnum tipoBebida) {
		StringBuilder sb = new StringBuilder("from Historico h ");
		sb.append("where h.bebida.tipoBebida = :tipoBebida  ");

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tipoBebida", tipoBebida.getCodigo());

		List<Historico> lista = recuperarPorQuery(sb.toString(), param);

		if (lista.isEmpty()) {
			return null;
		}
		return lista;
	}

}
