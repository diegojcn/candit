package br.com.estoque.dao;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import br.com.estoque.dao.generic.EstoqueGenericDAOImpl;
import br.com.estoque.domain.Secao;
import br.com.estoque.domain.TipoBebidaEnum;

/**
 * 
 * @author Diego Neves
 *
 */
@Stateless
public class ISecaoDAOImpl extends EstoqueGenericDAOImpl<Secao, Long> implements ISecaoDAOLocal {

	public List<Secao> pesquisarEstoqueComSecaoDisponivel(Integer capaciadadeDisponvivel,
			TipoBebidaEnum tipoBebidaEnum) {

		StringBuilder sb = new StringBuilder("from Secao s ");
		sb.append("where s.capacidadeDisponivel >= :capacidadeDisponivel  ");
		sb.append("and s.tipoBebida = :tipoBebida  ");

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("capacidadeDisponivel", capaciadadeDisponvivel);
		param.put("tipoBebida", tipoBebidaEnum.getCodigo());

		List<Secao> lista = recuperarPorQuery(sb.toString(), param);

		if (lista.isEmpty()) {
			return null;
		}
		return lista;
	}

	public Integer contarCapacidadePorBebida(TipoBebidaEnum tipoBebidaEnum) {

		StringBuilder sb = new StringBuilder("select sum(capacidadedisponivel)  from secao ");
		sb.append("where tipobebida = :tipoBebida  ");

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tipoBebida", tipoBebidaEnum.getCodigo());

		Query criarQueryNativa = criarQueryNativa(sb.toString(), param);
		BigInteger singleResult = (BigInteger) criarQueryNativa.getSingleResult();
		if (singleResult != null) {
			return singleResult.intValue();

		}
		return null;

	}

}
