package br.com.estoque.dao;

import javax.ejb.Stateless;

import br.com.estoque.dao.IBebidaDAOLocal;
import br.com.estoque.dao.generic.EstoqueGenericDAOImpl;
import br.com.estoque.domain.Bebida;

/**
 * 
 * @author Diego Neves
 *
 */
@Stateless
public class IBebidaDAOImpl extends EstoqueGenericDAOImpl<Bebida, Long> implements IBebidaDAOLocal {

}
