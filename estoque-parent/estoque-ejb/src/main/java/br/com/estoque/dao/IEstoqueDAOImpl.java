package br.com.estoque.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.estoque.dao.generic.EstoqueGenericDAOImpl;
import br.com.estoque.domain.Estoque;
import br.com.estoque.domain.TipoBebidaEnum;

/**
 * 
 * @author Diego Neves
 *
 */
@Stateless
public class IEstoqueDAOImpl extends EstoqueGenericDAOImpl<Estoque, Long> implements IEstoqueDAOLocal {


}
