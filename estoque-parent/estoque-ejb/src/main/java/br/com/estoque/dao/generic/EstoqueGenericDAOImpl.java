package br.com.estoque.dao.generic;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.estoque.dao.generic.IEstoqueGenericDAOLocal;
import br.com.estoque.domain.generic.EntidadeEstoque;
import br.com.framework.dao.GenericAbstractDAOImpl;

/**
 * 
 * @author d.neves
 *
 * @param <T>
 * @param <ID>
 */
@Stateless
public abstract class EstoqueGenericDAOImpl<T extends EntidadeEstoque, ID extends Serializable>
		extends GenericAbstractDAOImpl<T, ID> implements IEstoqueGenericDAOLocal<T, ID> {

	protected static final int QUANTIDADE_OBJETOS_AUTOCOMPLETE = 15;

	@Resource
	private EJBContext context;

	@PersistenceContext(unitName = "estoquePU")
	private EntityManager entityManager;

	public T salvarImediato(T entidade) {
		entidade = this.salvar(entidade);
		flush();
		return entidade;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
