/**
 * 
 */
package br.com.estoque.exception;

/**
 * @author d.neves
 *
 */
public class InternalErrorException extends EstoqueException {

	private static final long serialVersionUID = 3080578648721866362L;

	public InternalErrorException(String message, Object campo) {
		super(message);
	}

}
