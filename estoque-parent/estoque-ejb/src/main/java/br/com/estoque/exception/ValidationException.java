/**
 * 
 */
package br.com.estoque.exception;

/**
 * @author d.neves
 *
 */
public class ValidationException extends EstoqueException {
	
	private static final long serialVersionUID = 3080578648721866362L;
	private Object campo;

	public ValidationException(String message, Object campo) {
		super(message);
		this.campo = campo;
	}


	public Object getCampo() {
		return campo;
	}

	public void setCampo(Object campo) {
		this.campo = campo;
	}

}
