Projeto implementado utilizando Java8, EJB3, JPA e JaxRs.
O banco de dados foi o PostgresSQL, mas como está utilizando JPA fica fácil de testar em qualquer banco de dados.
Há uma classe de Testes.java que foi utilizada para testar o projeto.

A biblioteca do dn-framework-parent não estará disponível pois é uma lib pessoal utilizada em freelas.

Abaixo o DataSource do wildfly.

<datasource jndi-name="java:jboss/datasources/estoqueDS" pool-name="estoqueDS" enabled="true" use-java-context="true">
                    <connection-url>jdbc:postgresql://localhost:5432/estoque-DB</connection-url>
                    <driver>org.postgresql</driver>
                    <transaction-isolation>TRANSACTION_READ_COMMITTED</transaction-isolation>
                    <pool>
                        <min-pool-size>10</min-pool-size>
                        <max-pool-size>100</max-pool-size>
                        <prefill>true</prefill>
                        <flush-strategy>IdleConnections</flush-strategy>
                    </pool>
                    <security>
                        <user-name>postgres</user-name>
                        <password>sa</password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
                        <check-valid-connection-sql>select 1</check-valid-connection-sql>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>true</background-validation>
                        <background-validation-millis>60000</background-validation-millis>
                        <use-fast-fail>true</use-fast-fail>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
                    </validation>
                </datasource>